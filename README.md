1. #### **General description**
	The project includes backend of an online store.
2. #### **Requirements before running**
	- The application has been created using Java 8, Gradle, MySql and H2 databases, Spring and Hibernate frameworks, runs on the embedded Tomcat server. The detailed information is included in build.gradle.
	- Before running the application make sure you have the appropriate software installed. Credentials for the databases are listed in application.properties file. Remember to change the credentials in case you use other username, password, project name, also check the server timezone. When running the application make sure that your database is closed or uses different port.
	- The application includes a service of sending notifications to the admin email address when a new order has been created. By default the database is checked every 30 minutes, you can change the frequency in the EmailScheduler class. Make sure to set these fields of the application.property file before running: spring.mail.username and spring.mail.password are login and password of the email sender, admin.mail is a receiver of the email.
3. #### **Running the application**
	Before running the application build it using 'gradlew build' command in the terminal of your IDE. To start the application run a file called EcommerceeApplication situated in the package 'com.kodilla.ecommercee'.
4. #### **Endpoints**
    After running the application go to http://localhost:8080/swagger-ui.html for API documentation and comprehensive description of the endpoints.
5. #### **Utilization**
    The application is used to create and handle a database of an online store. It enables creation of products and product groups, users and their carts, allows making orders and track payments. The application may be developed to a holistic online store webpage by adding a frontend part.
6. #### **Test it live**
    The application has been deployed on [Heroku](https://www.heroku.com/). List of available endpoints under links [Swagger](https://online-store-573758.herokuapp.com/swagger-ui.html#!). You can test it by yourself on this [website](https://online-store-573758.herokuapp.com/login). 
7. #### **Secure**
    Important: Only Endpoint User is secured!
    
    1. Start Postman.
    2. Only Endpoint User is secured.
    3. First select the link with the method.
    4. Select "Authorization".
    5. Then select "Basic Auth".
    6. Enter your login and password:
        - role ADMIN:   login - kodilla_admin,
                        password - pass1,
        - role USER:    login - kodilla_user,
                        password - pass2.
    7. Enjoy. :)
    
    For the rest of the methods use "No Auth".

8. #### **Some screenshots demonstrating how the application works**
   ![App](/misc/GetAllOrders.png)
      
   ![App](/misc/getAllProducts.png)
      
   ![App](/misc/postAnOrders-request.png)
      
   ![App](/misc/postAnOrders-result.png)
      
   ![App](/misc/PostAProducts.png)
      
   ![App](/misc/putAProduct.png)    
      	
